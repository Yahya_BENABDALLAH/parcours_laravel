<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ActionController extends Controller
{
    public function RDVer($id)
    {
        try {

            ///// Partie RDV /////
            $RDV = DB::select('SELECT distinct count(rdv.id_rdv) as Rdv FROM rdv  WHERE id_emp=?', [$id]);
            $Pseudo = DB::select('SELECT Pseudo as Pseudo FROM emp where emp.ID = ?', [$id]);

            $CoefRdv = 40;
            $objectifRdv  = 3;

            $ResultatRdv =  $RDV[0]->Rdv / $objectifRdv;
            $ResultatFinaleRdv =  $ResultatRdv * $CoefRdv;

            ///// Partie Appels /////
            $NbrAppels = 1950;
            $objectifAppel = 0.00136;
            $TR = ($RDV[0]->Rdv) / $NbrAppels;
            $ResultatAppel = $TR / $objectifAppel;
            $CoefAppel = 35;
            $ResultatFinaleAppel = $CoefAppel * $ResultatAppel;

            ///// Partie Absence /////

            $debutSemaine = DB::select('SELECT (curdate() - INTERVAL((WEEKDAY(curdate()))) DAY) as Lundi');
            $finSemaine = DB::select('SELECT  (curdate() - INTERVAL((WEEKDAY(curdate()))-5) DAY) as Samedi');

            $JourPrevue = 5.5;
            $CoefAbsence = 25;
            $objectifAbsence  = 100;

            $JourAbsent = DB::select('SELECT count(id_abs) as Absence FROM absence  WHERE id_emp=? and date_abs between ? and ?', [$id, $debutSemaine[0]->Lundi, $finSemaine[0]->Samedi]);
            $JourTravailler = (($JourPrevue - ($JourAbsent[0]->Absence) / 2) / $JourPrevue) * 100;
            $ResultatAbsence =  $JourTravailler / $objectifAbsence;
            $ResultatFinaleAbsence =  $ResultatAbsence * $CoefAbsence;

            ///// Resultat final //////
            $Somme =  $ResultatFinaleRdv + $ResultatFinaleAppel + $ResultatFinaleAbsence;
            $Point = $Somme / ($CoefRdv + $CoefAppel + $CoefAbsence);

            if ($Point >= 0.95) $Classement = "A";
            if ($Point < 0.65) $Classement = "C";
            if ($Point < 0.95 && $Point >= 0.65) $Classement = "B";


            return [
                ///// Partie RDV /////
                "Pseudo" => $Pseudo[0]->Pseudo,
                "RDV" => $RDV[0]->Rdv,
                "Coef-rdv" => $CoefRdv,
                "Objectif-rdv" => $objectifRdv,
                "Resultat-rdv" => $ResultatRdv,
                "R.Finale-rdv" => $ResultatFinaleRdv,
                ///// Partie Appels /////
                "NbrApppels" => $NbrAppels,
                "RDV-bis" => $RDV[0]->Rdv,
                "Objectif-appel" => $objectifAppel,
                "TR" => $TR,
                "Resultat-appel" => $ResultatAppel,
                "Coef-appel" => $CoefAppel,
                "R.Finale-appel" => $ResultatFinaleAppel,
                ///// Partie Absence /////
                "Lundi" => $debutSemaine[0]->Lundi,
                "Samedi" => $finSemaine[0]->Samedi,
                "Absence" => $JourAbsent[0]->Absence,
                "Assiduite" => $JourTravailler,
                "Coef-abs" => $CoefAbsence,
                "Objectif-abs" => $objectifAbsence,
                "Resultat-abs" => $ResultatAbsence,
                "R.Finale-abs" => $ResultatFinaleAbsence,

                ///// Resultat final //////
                "Somme" => $Somme,
                "Point" => $Point,
                "Classement" => $Classement,

                " successfully imported"
            ];
        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }
    public function Agents()
    {
        try {

            $debutSemaine = DB::select('SELECT (curdate() - INTERVAL((WEEKDAY(curdate()))) DAY) as Lundi');
            $finSemaine = DB::select('SELECT  (curdate() - INTERVAL((WEEKDAY(curdate()))-5) DAY) as Samedi');
            $Agents = DB::select('SELECT Pseudo,ID,count(id_abs) as absence, count(id_rdv) as rdv
            from absence a
            inner join emp e on a.id_emp=e.ID
            inner join rdv r on r.id_emp=e.ID
            and  a.date_abs between ? and ? 
            group by Pseudo,ID
                            ', [$debutSemaine[0]->Lundi, $finSemaine[0]->Samedi]);
            return $Agents;
        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }
    public function Agent($id)
    {
        try {
            $Agent = DB::table('emp')->where('id', '=', [$id])->get();
            return $Agent;
        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }
    public function getAbsenceSemaine($id)
    {
        try {
            $debutSemaine = DB::select('SELECT (curdate() - INTERVAL((WEEKDAY(curdate()))) DAY) as Lundi');
            $finSemaine = DB::select('SELECT  (curdate() - INTERVAL((WEEKDAY(curdate()))-5) DAY) as Samedi');
            $JourAbsent = DB::table('absence')->where('id_emp', [$id])->whereBetween('date_abs', [$debutSemaine[0]->Lundi, $finSemaine[0]->Samedi])->count();
            return $JourAbsent;
        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }
    public function getRdvAgent($id)
    {
        try {
            $RDV = DB::table('rdv')->where('id_emp', [$id])->count();
            return $RDV;
        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }
    public function getData($id)
    {
        try {
            $Agent = DB::table('emp')->where('id', '=', [$id])->get();
            $RDV = DB::table('rdv')->where('id_emp', [$id])->count();
            $debutSemaine = DB::select('SELECT (curdate() - INTERVAL((WEEKDAY(curdate()))) DAY) as Lundi');
            $finSemaine = DB::select('SELECT  (curdate() - INTERVAL((WEEKDAY(curdate()))-5) DAY) as Samedi');
            $JourAbsent = DB::table('absence')->where('id_emp', [$id])->whereBetween('date_abs', [$debutSemaine[0]->Lundi, $finSemaine[0]->Samedi])->count();
            return [$Agent, $RDV, $JourAbsent];
        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }
}
