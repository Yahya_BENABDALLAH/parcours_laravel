-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le : jeu. 03 juin 2021 à 15:15
-- Version du serveur :  10.4.17-MariaDB
-- Version de PHP : 8.0.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `parcours`
--
CREATE DATABASE IF NOT EXISTS `parcours` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE `parcours`;

-- --------------------------------------------------------

--
-- Structure de la table `absence`
--

CREATE TABLE `absence` (
  `id` int(11) NOT NULL,
  `candidat` int(11) DEFAULT NULL,
  `formation` int(11) DEFAULT NULL,
  `dateAbsence` date DEFAULT NULL,
  `type` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `absence`
--

INSERT INTO `absence` (`id`, `candidat`, `formation`, `dateAbsence`, `type`) VALUES
(18, 6, 1, '2021-05-28', 1),
(19, 17, 1, '2021-05-28', 1),
(20, 6, 1, '2021-05-28', 2),
(21, 13, 1, '2021-05-28', 1),
(22, 13, 1, '2021-05-28', 2),
(23, 18, 1, '2021-05-28', 1),
(24, 18, 1, '2021-05-28', 2),
(25, 13, 1, '2021-05-28', 1);

-- --------------------------------------------------------

--
-- Structure de la table `archiveclassement`
--

CREATE TABLE `archiveclassement` (
  `id` int(11) NOT NULL,
  `Agent` text NOT NULL,
  `rdv` int(11) NOT NULL,
  `Appel` int(11) NOT NULL,
  `Absence` int(11) NOT NULL,
  `RDVFinale` float NOT NULL,
  `AppelFinale` float NOT NULL,
  `AbsenceFinale` float NOT NULL,
  `Point` text NOT NULL,
  `Classement` text NOT NULL,
  `Date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `candidat`
--

CREATE TABLE `candidat` (
  `id` int(11) NOT NULL,
  `nom` varchar(50) DEFAULT NULL,
  `prenom` varchar(50) DEFAULT NULL,
  `experience` text NOT NULL,
  `pseudo` int(11) DEFAULT NULL,
  `validation` int(11) DEFAULT NULL,
  `telephone` text NOT NULL,
  `CIN` text NOT NULL,
  `nationalite` text NOT NULL,
  `email` text NOT NULL,
  `adresse` text NOT NULL,
  `compagne` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `candidat`
--

INSERT INTO `candidat` (`id`, `nom`, `prenom`, `experience`, `pseudo`, `validation`, `telephone`, `CIN`, `nationalite`, `email`, `adresse`, `compagne`) VALUES
(6, 'Helene', 'Cadreal', '0', 1, NULL, '0123456789', 'F012345', 'MA', 'ABCD@noto.ma', 'Chariee', 1),
(7, 'Gala', 'RNG', '10', NULL, 1, '0123456789', 'F012345', 'MA', 'ABCD@noto.ma', 'Chariee', NULL),
(10, 'Bang', 'RNG', '0', NULL, NULL, '0123456789', 'F012345', 'SN', 'ABCD@noto.ma', 'Chariee', NULL),
(11, 'Zven', 'C9', '0', NULL, NULL, '0123456789', 'F012345', 'CN', 'ABCD@noto.ma', 'Chariee', NULL),
(12, 'Xiaho', 'RNG', '0', NULL, NULL, '0123456789', 'F012345', 'CN', 'ABCD@noto.ma', 'Chariee', NULL),
(13, 'SALS', 'SKT', '0', 1, NULL, '0123456789', 'F012345', 'CN', 'ABCD@noto.ma', 'Chariee', NULL),
(14, 'Koma', 'Skt', '0', NULL, NULL, '0123456789', 'F012345', 'USA', 'ABCD@noto.ma', 'Chariee', NULL),
(15, 'Ninja', 'Fortnite', '0', NULL, NULL, '0123456789', 'F012345', 'KR', 'ABCD@noto.ma', 'Chariee', NULL),
(16, 'Beng', 'SKT', '5', NULL, NULL, '0123456789', 'F012345', 'KR', 'ABCD@noto.ma', 'Chariee', NULL),
(17, 'Angel', 'SKT', '2', 1, NULL, '0123456789', 'F012345', 'AA', 'ABCD@noto.ma', 'Chariee', 2),
(18, 'Assia', 'Cadreal', '0', 1, NULL, '0123456789', 'F012345', 'MA', 'ABCD@noto.ma', 'Chariee', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `compagne`
--

CREATE TABLE `compagne` (
  `id` int(11) NOT NULL,
  `label` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `compagne`
--

INSERT INTO `compagne` (`id`, `label`) VALUES
(1, 'A'),
(2, 'B');

-- --------------------------------------------------------

--
-- Structure de la table `formation`
--

CREATE TABLE `formation` (
  `id` int(11) NOT NULL,
  `label` text NOT NULL,
  `type` int(11) DEFAULT NULL,
  `dateDebut` date DEFAULT NULL,
  `dateFin` date DEFAULT NULL,
  `max` int(11) DEFAULT NULL,
  `formationType` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `formation`
--

INSERT INTO `formation` (`id`, `label`, `type`, `dateDebut`, `dateFin`, `max`, `formationType`) VALUES
(1, 'Energie', 1, '2021-05-09', '2035-05-22', 110, 1),
(2, 'Energie', 1, '2023-06-04', '2025-05-22', 111, 2),
(3, 'Energie', 1, '2024-06-04', '2025-05-22', 80, 3),
(4, 'Box', 2, '2025-06-04', '2025-05-22', 90, 2),
(5, 'Box', 3, '2026-06-04', '2025-05-22', 90, 2),
(6, 'Box', 1, '2027-06-04', '2025-05-22', 10, 1),
(7, 'Telecom', 1, '2099-06-04', '2100-05-22', 99, 1),
(26, 'Telecom', 3, '2026-06-04', '2025-05-22', 90, 2),
(27, 'Telecom', 1, '2027-06-04', '2025-05-22', 10, 1),
(28, 'CPR', 1, '2099-06-04', '2100-05-22', 99, 1),
(29, 'CPR', 1, '2021-05-18', '2021-05-18', 1, 1),
(30, 'Energie', 1, '2021-05-12', '2021-05-18', 1, 1),
(52, 'TDM', 2, '2021-05-05', '2021-05-05', 555, 1),
(54, 'TDM', 2, '2021-05-21', '2021-05-06', 6666, 1),
(55, 'MAD', 1, '2021-05-22', '2021-05-28', 10, 1),
(56, 'RNG', 1, '2021-05-21', '2021-05-28', 10, 1),
(57, 'RNG', 1, '2021-05-22', '2021-05-28', 10, 1),
(58, 'C9', 1, '2021-05-21', '2021-05-28', 9, 1),
(59, 'TSM', 1, '2021-05-04', '2021-05-28', 10, 1),
(60, 'IMT', 1, '2021-04-26', '2021-04-26', 1, 1),
(61, 'DIG', 1, '2021-04-26', '2021-04-26', 1, 1),
(62, 'LOL', 1, '2021-04-07', '2021-03-31', 1, 1);

-- --------------------------------------------------------

--
-- Structure de la table `formationcandidat`
--

CREATE TABLE `formationcandidat` (
  `formation` int(11) NOT NULL,
  `candidat` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `formationcandidat`
--

INSERT INTO `formationcandidat` (`formation`, `candidat`) VALUES
(1, 6),
(1, 7);

-- --------------------------------------------------------

--
-- Structure de la table `notes`
--

CREATE TABLE `notes` (
  `id` int(11) NOT NULL,
  `label` varchar(50) DEFAULT NULL,
  `objectif` float DEFAULT NULL,
  `coef` int(11) DEFAULT NULL,
  `compagne` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `notes`
--

INSERT INTO `notes` (`id`, `label`, `objectif`, `coef`, `compagne`) VALUES
(1, 'absence', 0.00136, 35, 1),
(2, 'rdv', 0.00136, 35, 2);

-- --------------------------------------------------------

--
-- Structure de la table `pseudo`
--

CREATE TABLE `pseudo` (
  `id` int(11) NOT NULL,
  `label` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `pseudo`
--

INSERT INTO `pseudo` (`id`, `label`) VALUES
(1, 'Hyrkul'),
(2, 'Hunterzen10'),
(3, 'Qolys'),
(4, 'Incursio'),
(5, 'Yasmina'),
(6, 'Geek'),
(7, 'Suku'),
(8, 'Sakura');

-- --------------------------------------------------------

--
-- Structure de la table `score`
--

CREATE TABLE `score` (
  `id` int(11) NOT NULL,
  `date` date DEFAULT NULL,
  `Qte` int(11) DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `candidat` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `score`
--

INSERT INTO `score` (`id`, `date`, `Qte`, `type`, `candidat`) VALUES
(1, '2021-06-03', 4, 1, 17),
(2, '2021-06-03', 2, 2, 6),
(3, '2021-06-03', 2, 2, 17),
(4, '2021-06-03', 0, 1, 6),
(5, '2021-06-03', 3, 1, 6),
(6, '2021-06-03', 1, 1, 17),
(7, '2021-06-03', 1, 2, 17);

-- --------------------------------------------------------

--
-- Structure de la table `scoretype`
--

CREATE TABLE `scoretype` (
  `id` int(11) NOT NULL,
  `label` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `scoretype`
--

INSERT INTO `scoretype` (`id`, `label`) VALUES
(1, 'rdv'),
(2, 'Appel');

-- --------------------------------------------------------

--
-- Structure de la table `typeabsence`
--

CREATE TABLE `typeabsence` (
  `id` int(11) NOT NULL,
  `label` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `typeabsence`
--

INSERT INTO `typeabsence` (`id`, `label`) VALUES
(1, 'matin'),
(2, 'soir');

-- --------------------------------------------------------

--
-- Structure de la table `typeformation`
--

CREATE TABLE `typeformation` (
  `id` int(11) NOT NULL,
  `label` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `typeformation`
--

INSERT INTO `typeformation` (`id`, `label`) VALUES
(1, 'Integration'),
(2, 'plateau de formation'),
(3, 'plateau de production');

-- --------------------------------------------------------

--
-- Structure de la table `typeuser`
--

CREATE TABLE `typeuser` (
  `id` int(11) NOT NULL,
  `label` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `typeuser`
--

INSERT INTO `typeuser` (`id`, `label`) VALUES
(1, 'rh'),
(2, 'chef de plateau de formation'),
(3, 'chef de plateau de production'),
(4, 'chargee de recrutement');

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `pseudo` varchar(50) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  `nom` varchar(50) DEFAULT NULL,
  `prenom` varchar(50) DEFAULT NULL,
  `type` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `users`
--

INSERT INTO `users` (`id`, `pseudo`, `password`, `nom`, `prenom`, `type`) VALUES
(1, 'a', 'a', 'a', 'a', 1),
(2, 'b', 'b', 'b', 'b', 2),
(3, 'c', 'c', 'c', 'c', 3),
(4, 'd', 'd', 'd', 'd', 4);

-- --------------------------------------------------------

--
-- Structure de la table `validation`
--

CREATE TABLE `validation` (
  `id` int(11) NOT NULL,
  `label` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `validation`
--

INSERT INTO `validation` (`id`, `label`) VALUES
(1, 'Integration'),
(2, 'plateau de formation'),
(3, 'plateau de production');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `absence`
--
ALTER TABLE `absence`
  ADD PRIMARY KEY (`id`),
  ADD KEY `candidat` (`candidat`),
  ADD KEY `formation` (`formation`),
  ADD KEY `type` (`type`);

--
-- Index pour la table `archiveclassement`
--
ALTER TABLE `archiveclassement`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `candidat`
--
ALTER TABLE `candidat`
  ADD PRIMARY KEY (`id`),
  ADD KEY `pseudo` (`pseudo`),
  ADD KEY `validation` (`validation`),
  ADD KEY `compagne` (`compagne`);

--
-- Index pour la table `compagne`
--
ALTER TABLE `compagne`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `formation`
--
ALTER TABLE `formation`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_formationType` (`formationType`);

--
-- Index pour la table `formationcandidat`
--
ALTER TABLE `formationcandidat`
  ADD PRIMARY KEY (`formation`,`candidat`),
  ADD KEY `candidat` (`candidat`);

--
-- Index pour la table `notes`
--
ALTER TABLE `notes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_compagne` (`compagne`);

--
-- Index pour la table `pseudo`
--
ALTER TABLE `pseudo`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `score`
--
ALTER TABLE `score`
  ADD PRIMARY KEY (`id`),
  ADD KEY `type` (`type`),
  ADD KEY `FK_candidat` (`candidat`);

--
-- Index pour la table `scoretype`
--
ALTER TABLE `scoretype`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `typeabsence`
--
ALTER TABLE `typeabsence`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `typeformation`
--
ALTER TABLE `typeformation`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `typeuser`
--
ALTER TABLE `typeuser`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `type` (`type`);

--
-- Index pour la table `validation`
--
ALTER TABLE `validation`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `absence`
--
ALTER TABLE `absence`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT pour la table `archiveclassement`
--
ALTER TABLE `archiveclassement`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT pour la table `candidat`
--
ALTER TABLE `candidat`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT pour la table `compagne`
--
ALTER TABLE `compagne`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pour la table `formation`
--
ALTER TABLE `formation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=85;

--
-- AUTO_INCREMENT pour la table `notes`
--
ALTER TABLE `notes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pour la table `pseudo`
--
ALTER TABLE `pseudo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT pour la table `score`
--
ALTER TABLE `score`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT pour la table `scoretype`
--
ALTER TABLE `scoretype`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pour la table `typeabsence`
--
ALTER TABLE `typeabsence`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pour la table `typeformation`
--
ALTER TABLE `typeformation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT pour la table `typeuser`
--
ALTER TABLE `typeuser`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT pour la table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT pour la table `validation`
--
ALTER TABLE `validation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `absence`
--
ALTER TABLE `absence`
  ADD CONSTRAINT `absence_ibfk_1` FOREIGN KEY (`candidat`) REFERENCES `candidat` (`id`),
  ADD CONSTRAINT `absence_ibfk_2` FOREIGN KEY (`formation`) REFERENCES `formation` (`id`),
  ADD CONSTRAINT `absence_ibfk_3` FOREIGN KEY (`type`) REFERENCES `typeabsence` (`id`);

--
-- Contraintes pour la table `candidat`
--
ALTER TABLE `candidat`
  ADD CONSTRAINT `candidat_ibfk_1` FOREIGN KEY (`pseudo`) REFERENCES `pseudo` (`id`),
  ADD CONSTRAINT `candidat_ibfk_2` FOREIGN KEY (`validation`) REFERENCES `validation` (`id`),
  ADD CONSTRAINT `candidat_ibfk_3` FOREIGN KEY (`compagne`) REFERENCES `compagne` (`id`);

--
-- Contraintes pour la table `formation`
--
ALTER TABLE `formation`
  ADD CONSTRAINT `FK_formationType` FOREIGN KEY (`formationType`) REFERENCES `typeformation` (`id`);

--
-- Contraintes pour la table `formationcandidat`
--
ALTER TABLE `formationcandidat`
  ADD CONSTRAINT `formationcandidat_ibfk_1` FOREIGN KEY (`formation`) REFERENCES `formation` (`id`),
  ADD CONSTRAINT `formationcandidat_ibfk_2` FOREIGN KEY (`candidat`) REFERENCES `candidat` (`id`);

--
-- Contraintes pour la table `notes`
--
ALTER TABLE `notes`
  ADD CONSTRAINT `FK_compagne` FOREIGN KEY (`compagne`) REFERENCES `compagne` (`id`);

--
-- Contraintes pour la table `score`
--
ALTER TABLE `score`
  ADD CONSTRAINT `FK_candidat` FOREIGN KEY (`candidat`) REFERENCES `candidat` (`id`),
  ADD CONSTRAINT `score_ibfk_1` FOREIGN KEY (`type`) REFERENCES `scoretype` (`id`);

--
-- Contraintes pour la table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_ibfk_1` FOREIGN KEY (`type`) REFERENCES `typeuser` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
