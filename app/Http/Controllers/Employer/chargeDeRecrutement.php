<?php

namespace App\Http\Controllers\Employer;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class chargeDeRecrutement extends Controller
{

    public function getNewCandidat(Request $req)
    {
        try {
            if ($req->session()->has('user')) {
                $data = DB::select('SELECT id,nom,prenom,telephone,experience,CIN,nationalite,email,adresse,compagne FROM CANDIDAT where validation is NULL');
                return [
                    "data" => $data,
                    "Candidats successfully imported"
                ];
            } else return "Reconnectez-vous !";
        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }

    public function getFormationId( $id)
    {
        try {
            $now = DB::select('SELECT CURDATE()');
            // Code get Formation  
            $test = DB::select('select f.id, dateDebut,dateFin,max,f.label as Flabel,tf.label as formation from formation f , typeFormation tf where f.formationType=? and f.datedebut>CURDATE() and f.formationType=tf.id ORDER BY dateDebut', [$id]);
            return [
                "data" => $test,
                $now,
                "Formation successfully imported"
            ];
        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }

    public function UpdateFormationCandidat(Request $req)
    {
        foreach ($req->data as $user) {

            DB::table('candidat')
            ->where('id', '=', $user['id'])
                ->update([
                    'nom' => $user['nom'],
                    'prenom' => $user['prenom'],
                    "validation" => $req->validation
                ]);


            DB::table('formationcandidat')->insert([
                'candidat' => $user['id'],
                'formation' => $req->Formation,
            ]);
        }
    }
}
