<?php

namespace App\Http\Controllers\Employer;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class Formatrice extends Controller
{

    public function addFormation(Request $req)
    {
        try {
            DB::table('formation')
                ->insert([
                    'type' => $req->Type,
                    'dateDebut' => $req->DateDebut,
                    'dateFin' => $req->DateFin,
                    'max' => $req->Max,
                    'label' => $req->Label,
                    'formationType' => $req->FormationType
                ]);
            return "Formation successfully added";
        } catch (\Throwable $th) {

            return $th->getMessage();
        }
    }

    public function getFormation()
    {
        try {  
            $test = DB::select('select f.id, dateDebut,dateFin,max,f.label as formation from formation f , typeFormation tf where f.formationType=tf.id ORDER BY dateDebut');
            return [
                "data" => $test,
                "Formation successfully imported"
            ];
        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }
}