<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;

class emp extends Controller
{
    public function chargeederecrutementIndex()
    {
        return view('chargeederecrutement');
    }

    public function rhFormationIndex()
    {
        return view('rhformation');
    }

    public function rhCandidatIndex()
    {
        return view('rhcandidats');
    }

    public function rhAbsencesIndex()
    {
        return view('rhabsence');
    }

    public function formatriceIndex()
    {
        return view('formatrice', ['ID' => "1"]);
    }

    public function classementsIndex()
    {
        return view('classements');
    }
    
    
}
