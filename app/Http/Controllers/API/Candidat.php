<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class Candidat extends Controller
{
    public function getCandidat(Request $req)
    {
        try {
                $data = DB::select('SELECT c.id,c.nom,c.prenom,p.label as pseudo,v.label as validations FROM candidat c,pseudo p ,validation v 
                where c.pseudo=p.id
                and c.validation=v.id
                 ');
                    return [
                        "data" => $data,
                        "Candidats successfully imported"
                    ];

        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }

    public function getCandidatId(Request $req, $id) // Type Formatin id !!!
    {
        try {
            if ($req->session()->has('user')) {
                $data = DB::select('SELECT * FROM CANDIDAT where validation = ?', [$id]);
                return [
                    "data" => $data,
                    "Candidats list by id successfully imported"
                ];
            } else return "Reconnectez-vous !";
        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }

    public function updateCandidat(Request $req, $id)
    {
        try {
            if ($req->session()->has('user')) {
                if (session()->get('user')->type == 4) {
                    DB::table('candidat')
                        ->where('id', '=', $id)
                        ->update([
                            'nom' => $req->Nom,
                            'prenom' => $req->Prenom,
                            'pseudo' => $req->Pseudo,
                            'validation' => $req->Validation,
                        ]);
                    return "Candidat successfully updated";
                } else {
                    $req->session()->flush();
                    return "Vous n'etes pas autorisé";
                };
            } else return "Reconnectez-vous";
        } catch (\Throwable $th) {

            return $th->getMessage();
        }
    }

    public function updateCandidatValidation(Request $req, $id)
    {
        try {
            if ($req->session()->has('user')) {
                if (session()->get('user')->type == 1) {
                    DB::table('candidat')
                        ->where('id', '=', $id)
                        ->update([
                            'validation' => $req->Validation,
                        ]);
                    return "Candidat successfully updated";
                } else {
                    $req->session()->flush();
                    return "Vous n'etes pas autorisé";
                };
            } else return "Reconnectez-vous";
        } catch (\Throwable $th) {

            return $th->getMessage();
        }
    }

    public function UpdateFormationCandidat(Request $req)
    {
        try {
            $TypeFormation = DB::select(
                'SELECT t.label 
                from candidat C 
                inner join absence A on C.id=A.candidat 
                inner join formation f on a.formation=f.id 
                inner join typeformation t on f.formationType=t.id 
                where t.id=?',
                [$req->id]
            );
            return $TypeFormation;
        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }
}
