<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class Formation extends Controller
{

    public function addFormation(Request $req)
    {
        try {
            // if ($req->session()->has('user')) {
            // if (session()->get('user')->type == 1) {
            DB::table('formation')
                ->insert([
                    'type' => $req->Type,
                    'dateDebut' => $req->DateDebut,
                    'dateFin' => $req->DateFin,
                    'max' => $req->Max,
                    'label' => $req->Label,
                    'formationType' => $req->FormationType
                ]);
            //     } else {
            //         $req->session()->flush();
            //         return "Vous n'etes pas autorisé";
            //     };
            // } else return "Reconnectez-vous";
            return "Formation successfully added";
        } catch (\Throwable $th) {

            return $th->getMessage();
        }
    }

    public function getFormation()
    {
        try {
            // Code get Formation  
            $test = DB::select('select f.id, dateDebut,dateFin,max,f.label as formation as Type  from formation f , typeFormation tf where f.formationType=tf.id ORDER BY dateDebut');
            return [
                "data" => $test,
                "Formation successfully imported"
            ];
        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }


    public function updateFormation(Request $req, $id)
    {
        try {
            if ($req->session()->has('user')) {
                // Code update Formation  
                if (session()->get('user')->type == 1) {
                    DB::table('formation')
                        ->where('id', '=', $id)
                        ->update([
                            'type' => $req->Type,
                            'dateDebut' => $req->DateDebut,
                            'dateFin' => $req->DateFin,
                            'max' => $req->Max,
                            'formationType' => $req->FormationType
                        ]);
                    return "Formation successfully updated";
                } else {
                    $req->session()->flush();
                    return "Vous n'etes pas autorisé";
                };
            } else return "Reconnectez-vous";
        } catch (\Throwable $th) {

            return $th->getMessage();
        }
    }

    public function addTypeFormation(Request $req)
    {
        try {
            if ($req->session()->has('user')) {
                // Code Add Type Formation  
                if (session()->get('user')->type == 1) {

                    DB::table('typeformation')->insert([
                        'label' => $req->Label,
                    ]);
                    return "Type formation successfully inserted";
                } else {
                    $req->session()->flush();
                    return "Vous n'etes pas autorisé";
                };
            } else return "Reconnectez-vous";
        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }
}
