<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class Action extends Controller
{
    public function rdvAppel(Request $req)
    {
        try {
            $rdvappel = DB::select('SELECT  c.id as Agent ,nom,
                    ( select SUM(Qte) from score s where s.candidat = c.id and s.type=1 and YEAR(s.date) = YEAR(?) and MONTH(s.date) = MONTH(?)) as rdv,
                    ( select SUM(Qte) from score s where s.candidat = c.id and s.type=2 and YEAR(s.date) = YEAR(?)and MONTH(s.date) = MONTH(?)) as appel
                    FROM candidat c,validation v
                    where c.validation=v.id
                    and c.validation=2
                    ', [$req->date, $req->date, $req->date, $req->date]);
            return [
                "data" => $rdvappel,
                // "appel"=>$appel,
                "data successfully imported"
            ];
        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }

    public function AffectationPseudo(Request $req, $id)
    {
        try {
            if ($req->session()->has('user')) {
                if (session()->get('user')->type == 2) {
                    DB::table('candidat')
                        ->where('id', '=', $id)
                        ->update([
                            'pseudo' => $req->Pseudo,
                        ]);
                    return "Candidat Pseudo successfully updated";
                } else {
                    $req->session()->flush();
                    return "Vous n'etes pas autorisé";
                };
            } else return "Reconnectez-vous";
        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }
    
    public function AffectationFormation(Request $req, $id)
    {
        try {
            if ($req->session()->has('user')) {
                if (session()->get('user')->type == 4) {
                    DB::table('formationcandidat')->insert([
                        'candidat' => $id,
                        'formation' => $req->Formation,
                    ]);
                    $ObjectF = DB::table('formation')->where('id', $req->Formation)->get();
                    $typeF = $ObjectF[0]->formationType;
                    DB::table('candidat')
                        ->where('id', '=', $id)
                        ->update([
                            'validation' => $typeF,
                        ]);
                    return "Candidat Validation / Formation successfully updated";
                } else {
                    $req->session()->flush();
                    return "Vous n'etes pas autorisé";
                };
            } else return "Reconnectez-vous";
        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }
    
    public function listPseudo(Request $req)
    {
        try {
            if ($req->session()->has('user')) {
                if (session()->get('user')->type == 2) {

                    $data = DB::select('select * from pseudo');
                    return [
                        "data" => $data,
                        "Pseudos successfully imported"
                    ];
                } else {
                    $req->session()->flush();
                    return "Vous n'etes pas autorisé";
                };
            } else return "Reconnectez-vous";
        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }

    public function getTypeFormation()
    {
        try {
            $data = DB::select('SELECT id, label from typeformation');
            return $data;
        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }

    public function getFormationList()
    {
        try {
            $data = DB::select('SELECT
            ( select count(c.formation) from formationcandidat c where c.formation=f.id ) as "NBR candidats",
            f.id as ID ,
            tf.id as IDF,
            tf.label as "type formation",
            f.DateDebut as "Date Debut",
            f.DateFin "Date Fin"
                from formation f,typeformation tf
                where tf.id=f.formationType');
            return $data;
        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }

    public function pushData(Request $req)
    {
        $dataDecode = json_decode($req->data,true);
        foreach($dataDecode as $element){
        DB::table('archiveclassement')->insert([
            $element
        ]);
        }  
    }  
}
