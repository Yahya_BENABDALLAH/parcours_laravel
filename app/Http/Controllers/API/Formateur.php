<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class Formateur extends Controller
{
    public function Absent($id)
    {
        try {
            $data = DB::select(
                "select
                    c.id,
                    c.nom,
                    c.prenom,
                    fc.formation as  formation,
                    IF(EXISTS(select * from absence where absence.candidat=c.id and absence.dateAbsence = CURDATE() and type=1),true,false) as 'absence matin',
                    IF(EXISTS(select * from absence where absence.candidat=c.id and absence.dateAbsence = CURDATE() and type=2),true,false) as 'absence soir'
                from
                    candidat c
                inner join
                    formationcandidat fc on fc.candidat = c.id
                inner join
                    formation f on f.id = fc.formation
                where
                   curdate() between f.dateDebut and f.dateFin and  f.formationType=?",[$id] 
            );

            return [
                'etat' => 1,
                'data' => $data
            ];
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    public function toggleAbsent(Request $req)
    {

        try {
            $curdate = date("Y-m-d");
            $data = DB::select(
                "select
                    *
                from 
                    absence
                where
                    candidat = ? and type = ? and DATE(dateAbsence) = CURDATE()",
                [$req->ID, $req->type]
            );

            if (count($data) > 0) {
                DB::table("absence")
                    ->where([
                        ['candidat', $req->ID],
                        ['type', $req->type]
                    ])->whereDate(
                        'dateAbsence',
                        '=',
                        $curdate
                    )->delete();
            } else {
                DB::table('absence')
                    ->insert(
                        array(
                            'candidat' => $req->ID,
                            'type' => $req->type,
                            'formation' => $req->formation,
                            'dateAbsence' => $curdate
                        )
                    );
            }
            return [
                'etat' => 1,
                'Msg' => "Update absence effectuer.",
            ];
        } catch (\Throwable $th) {
            return [
                'etat' => 0,
                'error' => $th->getMessage()
            ];
        }
    }
}
