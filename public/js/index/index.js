function login() {
    var pseudo = document.querySelector("#Pseudo").value.trim();
    var password = document.querySelector("#Password").value.trim();
    var data = JSON.stringify({ Pseudo: pseudo, Password: password });
    console.log(data);
    $.ajax({
        method: "get",
        url: "/login",
        dataType: "json",
        data: { Pseudo: pseudo, Password: password },
        success: function (result) {
            if (!result.data) {
                console.log(result?.error || result?.isEmpty);
                return;
            } else {
                redirect(result.data.type);
            }
        },
    });
}

$("#Password").keyup(function (e) {
    if (e.keyCode == 13) {
        login();
    }
});