let selectedCandidat = [];
function candidatList() {
    $.ajax({
        url: "/getNewCandidat",
        method: "get",
        success: function (result) {
            console.log(result);
            theResult = result.data;
            currentpage = 1;
            lastIndexOfTable = 20;
            chargeePagination(theResult);
        },
    });
}
candidatList();

function getformations() {
    $.ajax({
        url: "/getFormationId/" + 1,
        method: "get",
        success: function (result) {
            formationToList(result.data);
            console.log(result);
        },
    });
}
getformations();

function updateFormationCandidat() {
    var selectedOption = document.getElementById("formationSelect");
    var affectationFormation =
        selectedOption.options[selectedOption.selectedIndex].value;
    console.log(selectedCandidat);
    console.log(affectationFormation);

    if (
        affectationFormation === "Choisir la formation" ||
        selectedCandidat.length === 0
    ) {
        if (affectationFormation === "Choisir la formation"){
            selectedOption.style.background = "#e0b9b9ba";
        }
    } else {
        $.ajax({
            url: "/UpdateFormationCandidat",
            method: "get",
            data: {
                data: selectedCandidat,
                Formation: affectationFormation,
                validation: 1,
            },
        });
        console.log("Hiii");
        candidatList();
        chargeePagination(theResult);
        document.getElementById("confirm").style.display = "none";
        selectedCandidat = [];
    }
}

document.querySelector("#search").addEventListener("input", (e) => {
    var filtredData = theResult.filter((abs) => {
        return (
            abs.id.toString().includes(e.target.value) ||
            abs.nom
                .toString()
                .toUpperCase()
                .includes(e.target.value.toString().toUpperCase()) ||
            abs.prenom
                .toString()
                .toUpperCase()
                .includes(e.target.value.toString().toUpperCase())
        );
    });
    chargeePagination(filtredData);
});

function formationToList(formation) {
    var select = `<select class="formationSelect" style=" font-weight:bold" id="formationSelect"><option selected disabled>Choisir la formation</option>`;
    formation.forEach((e) => {
        select =
            select +
            `\n<option value="${e.id}"}> - ID:${e.id} - Date Debut: ${e.dateDebut} - Label: ${e.Flabel}</option>`;
    });
    select = select + "</select>";
    // return select
    document.querySelector(".formationList").innerHTML = select;
}

function toggelSelect(ID) {
    if (document.querySelectorAll("#emp_" + ID + ":checked").length === 1) {
        var emp = theResult.find((e) => {
            return e.id == ID;
        });
        selectedCandidat = [...selectedCandidat, emp];
    } else {
        var emp = selectedCandidat.find((e) => {
            return e.id == ID;
        });
        selectedCandidat.splice(selectedCandidat.indexOf(emp), 1);
        console.log(selectedCandidat);
    }
}
