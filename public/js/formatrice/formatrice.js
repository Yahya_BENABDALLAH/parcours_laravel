// getAll List
function formationList() {
    $.ajax({
        url: "/getFormation",
        method: "get",
        success: function (result) {
            theResult = result.data;
            currentpage = 1;
            lastIndexOfTable = 20;
            formationPagination(theResult);
        },
    });
}
formationList();

$("#addFormationForm").submit(function (e) {
    e.preventDefault();
    var dateDebut = document.querySelector("#dateDebut").value;
    var dateFin = document.querySelector("#dateFin").value;
    var max = document.querySelector("#max").value;
    var type = document.querySelector("#type").value;
    var label = document.querySelector("#label").value;

    $.ajax({
        url: "/addFormation",
        method: "get",
        data: {
            Type: type,
            DateDebut: dateDebut,
            DateFin: dateFin,
            Max: max,
            Label: label,
            FormationType: 1,
        },
        success: function () {
            formationList();
            formationPagination(theResult);
            document.getElementById("expand").style.height = "0px";
            document.querySelector("#dateDebut").value = "";
            document.querySelector("#dateFin").value = "";
            document.querySelector("#max").value = "";
            document.querySelector("#type").value = "";
            document.querySelector("#label").value = "";
        },
    });
    closeConfirm();
});

// search
document.querySelector("#search").addEventListener("input", (e) => {
    var filtredData = theResult.filter((abs) => {
        return (
            abs.dateDebut.toString().toUpperCase().includes(e.target.value) ||
            abs.dateFin.toString().toUpperCase().includes(e.target.value)
        );
    });
    formationPagination(filtredData);
});
