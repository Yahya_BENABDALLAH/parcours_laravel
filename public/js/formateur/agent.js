const input = document.getElementById("formationList");
function getformations() {
    $.ajax({
        url: "/getTypeFormation",
        method: "get",
        success: function (result) {
            formationToList(result);
            console.log(result);
        },
    });
}
getformations();

function formationToList(formation) {
    var select = `<select class="formationSelect" style=" font-weight:bold" id="formationSelect">`;
    formation.forEach((e) => {
        select =
            select +
            `\n<option value="${e.id}" selected="selected">ID:${e.label}</option>`;
    });
    select = select + "</select>";
    // return select
    document.querySelector(".formationList").innerHTML = select;
}

function getAbsence() {
    $.ajax({
        url: "/Absent/1",
        success: function (result) {
            console.log(result);
            if (result.etat == 1) {
                currentpage2 = 1;
                lastIndexOfTable2 = 20;
                paginationTeamAb(result.data);
            } else {
            }
        },
    });
}
getAbsence();

function getAbsenceManuel(idFormation) {
    $.ajax({
        url: "/Absent/" + idFormation,
        success: function (result) {
            console.log(result);
            if (result.etat == 1) {
                currentpage2 = 1;
                lastIndexOfTable2 = 20;
                paginationTeamAb(result.data);
            }
        },
    });
}
input.addEventListener("change", () => {
    id = document.getElementById("formationSelect").value;
    getAbsenceManuel(id);
});

function checkAbs(ID, type, formation, element) {
    var result = element.hasAttribute("checked");
    var verification = confirm("are you sure ?!");
    if (!verification) {
        element.checked = result;
        return;
    }

    toggleAbs(ID, type, formation)
        .then((res) => {
            console.clear();
            console.log(res);
        })
        .catch((res) => {
            console.clear();
            console.log(res);
            element.checked = result;
        });
}

function toggleAbs(ID, type, formation) {
    return new Promise((resolve, reject) => {
        $.ajax({
            url: "/toggle",
            data: {
                ID: ID,
                type: type,
                formation: formation,
            },
            success: function (result) {
                if (result.etat == 1) {
                    resolve({ etat: 1 });
                } else {
                    reject({ etat: 0 });
                }
            },
        });
    });
}
