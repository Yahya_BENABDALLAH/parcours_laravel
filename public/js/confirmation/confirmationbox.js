
var confirm = document.getElementById("confirm");
// Get the button that opens the modal
var btn = document.getElementById("ValiderBtn");
// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];
// When the user clicks on the button, open the modal
btn.onclick = function() {
  confirm.style.display = "block";
}
// When the user clicks on <span> (x), close the modal
span.onclick = function() {
  confirm.style.display = "none";
}
// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
  if (event.target == confirm) {
    confirm.style.display = "none";
  }
}
function closeConfirm() {
    document.getElementById("confirm").style.display = "none";
}