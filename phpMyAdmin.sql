-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 11, 2021 at 01:10 PM
-- Server version: 10.4.17-MariaDB
-- PHP Version: 8.0.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `parcours`
--
CREATE DATABASE IF NOT EXISTS `parcours` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE `parcours`;

-- --------------------------------------------------------

--
-- Table structure for table `absence`
--

CREATE TABLE `absence` (
  `id` int(11) NOT NULL,
  `candidat` int(11) DEFAULT NULL,
  `formation` int(11) DEFAULT NULL,
  `dateAbsence` date DEFAULT NULL,
  `type` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `absence`
--

INSERT INTO `absence` (`id`, `candidat`, `formation`, `dateAbsence`, `type`) VALUES
(1, 6, 1, '2021-05-04', 2),
(2, 7, 1, '2021-05-04', 2);

-- --------------------------------------------------------

--
-- Table structure for table `candidat`
--

CREATE TABLE `candidat` (
  `id` int(11) NOT NULL,
  `nom` varchar(50) DEFAULT NULL,
  `prenom` varchar(50) DEFAULT NULL,
  `pseudo` int(11) DEFAULT NULL,
  `validation` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `candidat`
--

INSERT INTO `candidat` (`id`, `nom`, `prenom`, `pseudo`, `validation`) VALUES
(6, 'Perkz', 'Cadreal', 1, 1),
(7, 'Perkz', 'Zven', 2, 3),
(10, 'Faker', 'M5', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `formation`
--

CREATE TABLE `formation` (
  `id` int(11) NOT NULL,
  `type` int(11) DEFAULT NULL,
  `dateDebut` date DEFAULT NULL,
  `dateFin` date DEFAULT NULL,
  `max` int(11) DEFAULT NULL,
  `formationType` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `formation`
--

INSERT INTO `formation` (`id`, `type`, `dateDebut`, `dateFin`, `max`, `formationType`) VALUES
(1, 1, '2021-05-09', '2035-05-22', 110, 1),
(2, 1, '2023-06-04', '2025-05-22', 111, 2),
(3, 1, '2024-06-04', '2025-05-22', 80, 3),
(4, 2, '2025-06-04', '2025-05-22', 90, 2),
(5, 3, '2026-06-04', '2025-05-22', 90, 2),
(6, 1, '2027-06-04', '2025-05-22', 10, 1),
(7, 1, '2099-06-04', '2100-05-22', 99, 1);

-- --------------------------------------------------------

--
-- Table structure for table `formationcandidat`
--

CREATE TABLE `formationcandidat` (
  `formation` int(11) NOT NULL,
  `candidat` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `formationcandidat`
--

INSERT INTO `formationcandidat` (`formation`, `candidat`) VALUES
(1, 6),
(1, 7),
(2, 6),
(2, 7),
(6, 7),
(6, 10);

-- --------------------------------------------------------

--
-- Table structure for table `pseudo`
--

CREATE TABLE `pseudo` (
  `id` int(11) NOT NULL,
  `label` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `pseudo`
--

INSERT INTO `pseudo` (`id`, `label`) VALUES
(1, 'Hyrkul'),
(2, 'Hunterzen10');

-- --------------------------------------------------------

--
-- Table structure for table `typeabsence`
--

CREATE TABLE `typeabsence` (
  `id` int(11) NOT NULL,
  `label` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `typeabsence`
--

INSERT INTO `typeabsence` (`id`, `label`) VALUES
(1, 'matin'),
(2, 'soir');

-- --------------------------------------------------------

--
-- Table structure for table `typeformation`
--

CREATE TABLE `typeformation` (
  `id` int(11) NOT NULL,
  `label` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `typeformation`
--

INSERT INTO `typeformation` (`id`, `label`) VALUES
(1, 'formation'),
(2, 'plateau de formation'),
(3, 'plateau de production');

-- --------------------------------------------------------

--
-- Table structure for table `typeuser`
--

CREATE TABLE `typeuser` (
  `id` int(11) NOT NULL,
  `label` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `typeuser`
--

INSERT INTO `typeuser` (`id`, `label`) VALUES
(1, 'recruteur'),
(2, 'formateur'),
(3, 'chef de plateau de formation'),
(4, 'chef de plateau de production');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `pseudo` varchar(50) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  `nom` varchar(50) DEFAULT NULL,
  `prenom` varchar(50) DEFAULT NULL,
  `type` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `pseudo`, `password`, `nom`, `prenom`, `type`) VALUES
(1, 'a', 'a', 'a', 'a', 1),
(2, 'b', 'b', 'b', 'b', 2),
(3, 'c', 'c', 'c', 'c', 3);

-- --------------------------------------------------------

--
-- Table structure for table `validation`
--

CREATE TABLE `validation` (
  `id` int(11) NOT NULL,
  `label` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `validation`
--

INSERT INTO `validation` (`id`, `label`) VALUES
(1, 'formation'),
(2, 'plateau de formation'),
(3, 'plateau de production');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `absence`
--
ALTER TABLE `absence`
  ADD PRIMARY KEY (`id`),
  ADD KEY `candidat` (`candidat`),
  ADD KEY `formation` (`formation`),
  ADD KEY `type` (`type`);

--
-- Indexes for table `candidat`
--
ALTER TABLE `candidat`
  ADD PRIMARY KEY (`id`),
  ADD KEY `pseudo` (`pseudo`),
  ADD KEY `validation` (`validation`);

--
-- Indexes for table `formation`
--
ALTER TABLE `formation`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_formationType` (`formationType`);

--
-- Indexes for table `formationcandidat`
--
ALTER TABLE `formationcandidat`
  ADD PRIMARY KEY (`formation`,`candidat`),
  ADD KEY `candidat` (`candidat`);

--
-- Indexes for table `pseudo`
--
ALTER TABLE `pseudo`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `typeabsence`
--
ALTER TABLE `typeabsence`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `typeformation`
--
ALTER TABLE `typeformation`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `typeuser`
--
ALTER TABLE `typeuser`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `type` (`type`);

--
-- Indexes for table `validation`
--
ALTER TABLE `validation`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `absence`
--
ALTER TABLE `absence`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `candidat`
--
ALTER TABLE `candidat`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `formation`
--
ALTER TABLE `formation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `pseudo`
--
ALTER TABLE `pseudo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `typeabsence`
--
ALTER TABLE `typeabsence`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `typeformation`
--
ALTER TABLE `typeformation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `typeuser`
--
ALTER TABLE `typeuser`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `validation`
--
ALTER TABLE `validation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `absence`
--
ALTER TABLE `absence`
  ADD CONSTRAINT `absence_ibfk_1` FOREIGN KEY (`candidat`) REFERENCES `candidat` (`id`),
  ADD CONSTRAINT `absence_ibfk_2` FOREIGN KEY (`formation`) REFERENCES `formation` (`id`),
  ADD CONSTRAINT `absence_ibfk_3` FOREIGN KEY (`type`) REFERENCES `typeabsence` (`id`);

--
-- Constraints for table `candidat`
--
ALTER TABLE `candidat`
  ADD CONSTRAINT `candidat_ibfk_1` FOREIGN KEY (`pseudo`) REFERENCES `pseudo` (`id`),
  ADD CONSTRAINT `candidat_ibfk_2` FOREIGN KEY (`validation`) REFERENCES `validation` (`id`);

--
-- Constraints for table `formation`
--
ALTER TABLE `formation`
  ADD CONSTRAINT `FK_formationType` FOREIGN KEY (`formationType`) REFERENCES `typeformation` (`id`);

--
-- Constraints for table `formationcandidat`
--
ALTER TABLE `formationcandidat`
  ADD CONSTRAINT `formationcandidat_ibfk_1` FOREIGN KEY (`formation`) REFERENCES `formation` (`id`),
  ADD CONSTRAINT `formationcandidat_ibfk_2` FOREIGN KEY (`candidat`) REFERENCES `candidat` (`id`);

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_ibfk_1` FOREIGN KEY (`type`) REFERENCES `typeuser` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
