<!DOCTYPE html>
<html lang="en">

<head>
    <link href="https://fonts.googleapis.com/css?family=Raleway&display=swap" rel="stylesheet">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.5/jquery.min.js"></script>
    <input type="checkbox" name="" id="">
    <link rel="stylesheet" href="/styles/nav/style.css">
    <link rel="stylesheet" href="/styles/formateur/style.css">
    <title>
        Absence
    </title>
</head>

<body>
    <nav>
        <div class="logo"></div>
        <div class="navBtns">
            <input type="button" value="Candidats" class="first" name="Candidats" onclick="openBTcandidat()">
            <input type="button" value="Formations" class="first" name="Formations" onclick="openBTformation()">
            <input type="button" value="Classement" class="first" name="Classements" onclick="openBTClassement()">

        </div>
        <div class="logoutContainer">
            <input type="button" value="Se deconnecter" onclick="logout()">
        </div>
    </nav>

    <main class="">
        <div class="salaire sectionAcceuil">
        </div>
        <div class="Absence sectionAcceuil Title">
            <h1>Les absences journalies des candidats</h1>
            <div class="formationList" id="formationList">
            </div>
            <div class="content">
            </div>
            <div class="pages"></div>
        </div>
    </main>

</body>
<script src="/js/redirections/redirections.js" sync></script>
<script src="/js/formateur/agent.js" sync></script>
<script src="/js/formateur/pagination.js" sync></script>
<script src="/js/formateur/paginationTeamAb.js" sync></script>

</html>