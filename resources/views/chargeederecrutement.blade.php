<!DOCTYPE html>
<html lang="en">

<head>
    <link href="https://fonts.googleapis.com/css?family=Raleway&display=swap" rel="stylesheet">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.5/jquery.min.js"></script>
    <link rel="stylesheet" href="/styles/chargeederecrutement/style.css">
    <link rel="stylesheet" href="/styles/nav/style.css">
    <title>
        Chargée de recrutement
    </title>
</head>

<body>
    <nav>
        <div class="logo"></div>
        <div class="logoutContainer">
            <input type="button" value="Se deconnecter">
        </div>
    </nav>
    <main class="">
        <div class="Title">
            <h1>Liste des nouveaux candidats</h1>
            <input type="text" name="" id="search" placeholder=" Chercher ...">
        </div>
        <div class="formationContainer">
            <div class="formationList">
            </div>
            <input class="ValiderBtn" id="ValiderBtn" type="button" value="Valider" onclick="document.getElementById('confirm').style.display='block'">

        </div>
        <div id="confirm" class="confirm">
            <div class="modal-content">
                <span class="close">&times;</span>
                <p style="padding-bottom:5px ;">Êtes-vous sûr de vouloir continuer ?</p>
                <button class="BtnConfirmValider" onclick="updateFormationCandidat()">✔</button>
                <button class="BtnConfirmAnnuler" onclick="closeConfirm()">✖</button>
            </div>
        </div>
        <div class="content">
        </div>
        <div class="pages">
        </div>
    </main>
</body>

<script src="/js/chargeederecrutement/chargeederecrutement.js" sync></script>
<script src="/js/redirections/redirections.js" sync></script>
<script src="/js/chargeederecrutement/chargeepagination.js" sync></script>
<script src="/js/confirmation/confirmationbox.js" sync></script>