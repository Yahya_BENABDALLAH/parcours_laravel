<!DOCTYPE html>
<html lang="en">

<head>
    <link href="https://fonts.googleapis.com/css?family=Raleway&display=swap" rel="stylesheet">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.5/jquery.min.js"></script>
    <link rel="stylesheet" href="/styles/formatrice/style.css">
    <link rel="stylesheet" href="/styles/nav/style.css">
    <title>
        Formations
    </title>
</head>

<body>
    <nav>
        <div class="logo"></div>
        <div class="navBtns">
            <input type="button" value="Candidats" class="first" name="Candidats" onclick="openBTcandidat()">
            <input type="button" value="Classement" class="first" name="Classements" onclick="openBTClassement()">
            <input type="button" value="Absence" class="first" name="Absence" onclick="openBTabsence()">
        </div>
        <div class=" logoutContainer">
            <input type="button" value="Se deconnecter">
        </div>
    </nav>
    <main class="">
        <div class="Title">
            <h1>Liste des formations</h1>
            <div class="formationList" id="formationList">
            </div>
            <input type="text" name="" id="search" placeholder=" Chercher ...">
        </div>
        <div class="content">
        </div>
        <div class="pages">
        </div>

    </main>
</body>

<script src="/js/formatrice/formatrice.js" sync></script>
<script src="/js/redirections/redirections.js" sync></script>
<script src="/js/formatrice/formatricePagination.js" sync></script>