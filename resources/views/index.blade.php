<!DOCTYPE html>
<html lang="en">

<head>
    <link href="https://fonts.googleapis.com/css?family=Raleway&display=swap" rel="stylesheet">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.5/jquery.min.js"></script>
    <link rel="stylesheet" href="styles/index/style.css">
    <title>Document</title>
</head>

<body>
    <div class="loginContainer">
        <label for="Pseudo">Pseudo</label>
        <input type="text" name="" id="Pseudo">
        <label for="Password">Mot de passe</label>
        <input type="password" name="" id="Password" autocomplete=FALSE>
        <button onclick="login()">Se connecter</button>
    </div>
    <script src="js/index/index.js" sync></script>
    <script src="/js/redirections/redirections.js" sync></script>
</body>

</html>