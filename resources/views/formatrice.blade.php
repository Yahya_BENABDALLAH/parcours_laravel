<!DOCTYPE html>
<html lang="en">

<head>
    <link href="https://fonts.googleapis.com/css?family=Raleway&display=swap" rel="stylesheet">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.5/jquery.min.js"></script>
    <link rel="stylesheet" href="/styles/formatrice/style.css">
    <link rel="stylesheet" href="/styles/nav/style.css">
    <title>
        Formations
    </title>
</head>

<body>
    <nav>
        <div class="logo"></div>
        <div class="navBtns">
            <input type="button" value="Candidats" class="first" name="Candidats" onclick="openBTcandidat()">
            <input type="button" value="Absence" class="first" name="Absence" onclick="openBTabsence()">
        </div>
        <div class="logoutContainer">
            <input type="button" value="Se deconnecter">
        </div>
    </nav>
    <main class="">
        <div class="Title">
            <h1>Liste des formations</h1>

            <input type="text" name="" id="search" placeholder=" Chercher ...">
            <button class="openbtn" id="openbtn" onclick=openForm()>
                ➕ Ajouter Formation</button>
        </div>

        <div id="expand" class="expand" style="height: 0px">
            <form id="addFormationForm">
                <label class="labelText">Label</label>
                <input type="text" name="label" id="label" oninvalid="closeConfirm()" required><br>
                <label class="labelText">Date debut</label>
                <input type="date" name="dateDebut" id="dateDebut" oninvalid="closeConfirm()" required><br>
                <label class="labelText">Date Fin </label>
                <input type="date" name="dateFin" id="dateFin" oninvalid="closeConfirm()" required><br>
                <label class="labelText">Type </label>
                <input type="number" min="1" name="type" id="type" oninvalid="closeConfirm()" placeholder="Type formation ..." required><br>
                <label class="labelText" palce>Max </label>
                <input type="number" min="1" name="max" id="max" oninvalid="closeConfirm()" placeholder="Nbr candidats ..." required><br>
                <button type="button" id="ValiderBtn" class="ValiderBtn">Ajouter</button><input type="reset" id="ResetBtn" class="ResetBtn" />
                <div id="confirm" class="confirm">
                    <div class="modal-content">
                        <span class="close">&times;</span>
                        <p style="padding-bottom:5px ; color: white; font-weight: normal;">Êtes-vous sûr de vouloir continuer ?</p>
                        <button type="submit" class="BtnConfirmValider" style="background-color: #fefefe; border-radius: 5px; cursor: pointer;">✔</button>
                        <button class="BtnConfirmAnnuler" style="background-color: #fefefe; border-radius: 5px;cursor: pointer;" onclick="closeConfirm()">✖</button>
                    </div>
                </div>
            </form>
        </div>
        <div class=" content">
                    </div>
                    <div class="pages">
                    </div>

    </main>
</body>

<script src="/js/formatrice/formatrice.js" sync></script>
<script src="/js/redirections/redirections.js" sync></script>
<script src="/js/formatrice/formatricePagination.js" sync></script>
<script src="/js/confirmation/confirmationbox.js" sync></script>