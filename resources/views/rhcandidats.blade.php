<!DOCTYPE html>
<html lang="en">

<head>
    <link href="https://fonts.googleapis.com/css?family=Raleway&display=swap" rel="stylesheet">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="/styles/rhcandidats/style.css">
    <link rel="stylesheet" href="/styles/nav/style.css">
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.5/jquery.min.js"></script>
    <title>

    </title>
</head>

<body>
    <nav>
        <div class="logo"></div>
        <div class="navBtns">
            <input type="button" value="Formations" class="first" name="Formations" onclick="openBTformation()">
            <input type="button" value="Absence" class="first" name="Absence" onclick="openBTabsence()">
            <input type="button" value="Classement" class="first" name="Classements" onclick="openBTClassement()">
        </div>
        <div class="logoutContainer">
            <input type="button" value="Se deconnecter">
        </div>
    </nav>
    <main class="">
        <h1>La liste des candidats</h1>
        <input type="text" id="search" placeholder=" Chercher...">
        <div class="content"></div>
        <div class="pages"></div>
    </main>

</body>

<script src="/js/rhcandidats/rhcandidat.js" sync></script>
<script src="/js/rhcandidats/rhcandidatPagination.js" sync></script>
<script src="/js/redirections/redirections.js" sync></script>