<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    session()->forget('user');
    session()->flush();
    return view('index');
});
// Authentification
Route::get("/login", 'API\Connexion@login');
Route::get("/logout", 'API\Connexion@logout');

// Formation routes 
Route::get("/updateFormation/{id}", 'API\Formation@updateFormation');
Route::get("/addTypeFormation", 'API\Formation@addTypeFormation');
Route::get("/getTypeFormation", 'API\Action@getTypeFormation');


// Absences routes
Route::get("/getAbsenceType/{id}", 'API\Absence@getAbsenceType'); // Absence by id Formation
Route::get("/listAbsence", 'API\Absence@listAbsence'); // All absences

// Candidats routes

Route::get("/getCandidat", 'API\Candidat@getCandidat');

Route::get("/getCandidatId/{id}", 'API\Candidat@getCandidatId');
Route::get("/updateCandidat/{id}", 'API\Candidat@updateCandidat');
Route::get("/updateCandidatValidation/{id}", 'API\Candidat@updateCandidatValidation');

// Actions routes
Route::get("/listPseudo", 'API\Action@listPseudo');
Route::get("/AffectationPseudo/{id}", 'API\Action@AffectationPseudo');
Route::get("/AffectationFormation/{id}", 'API\Action@AffectationFormation');
Route::get("/getFormationList", 'API\Action@getFormationList');
Route::get("/pushData", 'API\Action@pushData');
Route::get("/rdvAppel", 'API\Action@rdvAppel');
Route::get("/getTypeFormation", 'API\Action@getTypeFormation');

// Recrutement
Route::get("/chargeederecrutement", 'API\emp@chargeederecrutementIndex');

// Chargee de Recrutements routes
Route::get("/getNewCandidat", 'Employer\chargeDeRecrutement@getNewCandidat');
Route::get("/getFormationId/{id}", 'Employer\chargeDeRecrutement@getFormationId');
Route::get("/UpdateFormationCandidat", 'Employer\chargeDeRecrutement@UpdateFormationCandidat');

// Formatrice routes
Route::get(
    "/formation",
    'API\emp@formatriceIndex'
);
Route::get("/addFormation", 'Employer\formatrice@addFormation');
Route::get("/getFormation", 'Employer\formatrice@getFormation');

// Rh routes
Route::get(
    "/rh",
    'API\emp@rhCandidatIndex'
);
Route::get(
    "/rhFormation",
    'API\emp@rhFormationIndex'
);
Route::get(
    "/rhAbsences",
    'API\emp@rhAbsencesIndex'
);

// Classements
Route::get(
    "/classements",
    'API\emp@classementsIndex'
);

Route::get(
    "/Absent/{id}",
    'API\Formateur@Absent'
);

Route::get(
    "/toggle",
    'API\Formateur@toggleAbsent'
);
